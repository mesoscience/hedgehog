#This is an input file for Hedgehog for the 23 March 2016 CHiMaD annual meeting.  It uses Cahn-Hilliard dynamics in the split formulation.  Units are in aJ and nm.  Elasticity is included; the material is modeled as linear elastic.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 12
  ny = 12
  xmin = 500
  xmax = 1000
  ymin = 500
  ymax = 1000

  elem_type = QUAD4
[]

#[MeshModifiers]
#  [./AddExtraNodeset]
#    #this is done to fix a node for mechanical computations
#    new_boundary = 100
#    coord = '0 0'
#    type = AddExtraNodeset
#  [../]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1.016
      outvalue = 0.0165
      radius = 50
      int_width = 5
      x1 = 500
      y1 = 500
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_x]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_y]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./elastic_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./interface_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./total_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s11_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s12_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s22_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e11_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e12_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e22_aux]
    order = FIRST
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./TensorMechanics]
    displacements = 'disp_x disp_y'
  [../]

  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltElasticity
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_elastic_energy]
    type = ElasticEnergy
    variable = elastic_energy
  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    elastic_energy = elastic_energy
    variable = total_energy
  [../]

  [./matl_s11]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 0
    variable = s11_aux
  [../]

 [./matl_s12]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 1
    variable = s12_aux
  [../]

  [./matl_s22]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 1
    index_j = 1
    variable = s22_aux
  [../]

  [./matl_e11]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 0
    variable = e11_aux
  [../]

  [./matl_e12]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 1
    variable = e12_aux
  [../]

  [./matl_e22]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 1
    index_j = 1
    variable = e22_aux
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltBaseMaterial
    AC_mobility = 5
    well_height = 2.592e-1
    kappa_AC = 8.1e-1

    order_parameter = eta1
  [../]

  [./gamma_gammaprime]
    type = HeterogeneousLinearElasticMaterial

    #reading C_11 C_12 C_13 C_22 C_23 C_33 C_44 C_55 C_66
    #units of aJ/nm^3
    Cijkl_matrix = '229 165 165 229 165 229 97.3 97.3 97.3'
    Cijkl_precip = '272 158 158 272 158 272 162 162 162'
    precipitate_eigenstrain = '0.0053 0.0053 0.0053 0 0 0'

    order_parameter = eta1
    disp_x = disp_x
    disp_y = disp_y
  [../]
[]

[BCs]
  [./Stress_dispx]
    type = StressBC
    variable = disp_x
    component = 0
    boundary_stress = '0 0.1 0 0 0 0'
    boundary = 'top'
  [../]

  [./Stress_dispy]
    type = StressBC
    variable = disp_y
    component = 1
    boundary_stress = '0 0.1 0 0 0 0'
    boundary = 'top'
  [../]

#  [./Stress_dispz]
#    type = StressBC
#    variable = disp_z
#    component = 2
#    boundary_stress = '0 0.1 0 0 0 0'
#    boundary = 'top bottom'
#  [../]

 [./pin_nodex]
    type = DirichletBC
    variable = disp_x
    value = 0.0
    boundary = 'left'
  [../]

 [./pin_nodey]
    type = DirichletBC
    variable = disp_y
    value = 0.0
    boundary = 'bottom'
  [../]
[]

[Postprocessors]
  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
  [./SystemFreeEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
 [./numDOFs]
    type = NumDOFs
  [../]
  [./VolumeFraction]
    type = NodalVolumeFraction
 #  bubble_volume_file = 1D_interfaceEnergy_vol.csv
    threshold = 0.5
    variable = eta1
    mesh_volume = Volume
  [../]
[]

[Adaptivity]
  marker = combo
  initial_steps = 5
  max_h_level = 5
  [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.5
      indicator = GJI_1
    [../]

    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.25
      indicator = GJI_2
    [../]

    [./EFM_3]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.25
      indicator = GJI_3
    [../]

    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2 EFM_3'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = eta1
    [../]

    [./GJI_2]
     type = GradientJumpIndicator
     variable = disp_x
    [../]

    [./GJI_3]
     type = GradientJumpIndicator
     variable = disp_y
    [../]
  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e2
    percent_change = 0.05
  [../]

  num_steps = 5000
  dtmin = 1e-2
  #dtmax = 1e2

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -pc_asm_overlap -sub_pc_type -ksp_gmres_restart'
  petsc_options_value = ' asm      4               lu           30'

  l_tol = 1e-4
  l_max_its = 50
  nl_max_its = 20
  nl_rel_tol = 1e-6
  nl_abs_tol = 1e-10
  ss_check_tol = 1e-8
  trans_ss_check = 1
[]


[Outputs]
  interval = 5
  csv = true
  file_base = hedgehog_300K_p100MPa_v2
  checkpoint = true

  [./console]
    type = Console
    perf_log = true
  [../]

  [./my_exodus]
    type = Exodus
    use_problem_dimension = false
  [../]
[]
