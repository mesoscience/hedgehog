#This simulation is in 1D to find the interfacial energy, energy/length^2.
#Units are going to need to be re-dimensionalised.
#To use this, get the equilibrium omega value first by setting omega_eq = 0 and allowing the simulation to run to equilibrium.  The omega value at either end of the simulation should be the same, and that's omega_eq.  Then run the simulation again to find the interface energy in J/unit^2 (nm here) by supplying omega_eq to the auxkernel.  It outputs as gamma, the postprocessor

#Note that the auxkernel does not include a non-zero g(eta) term in the free energy.

[Mesh]
  type = GeneratedMesh
  dim = 3
  nx = 12
  ny = 12
  nz = 12
  xmin = 0
  xmax = 100
  ymin = 0
  ymax = 100
  zmin = 0
  zmax = 100
  elem_type = HEX8
  uniform_refine = 2
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      variable = eta1
      type = SmoothCircleIC
      invalue = 1.03
      outvalue = 0.035
      radius = 15
      int_width = 5
      x1 = 50
      y1 = 50
      z1 = 50
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./interface_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    variable = total_energy
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    off_diag_row = 'eta1 mu'
    off_diag_column = 'mu eta1'
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltBase
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltBaseMaterial
    AC_mobility = 1
    well_height = 7.2e-2
    kappa_AC = 2.25e-1

    order_parameter = eta1
  [../]
[]

[Postprocessors]
  [./dofs]
   type = NumDOFs
  [../]

  [./SystemFreeEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]

  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]

  [./VolumeFraction]
    type = NodalVolumeFraction
 #  bubble_volume_file = 1D_interfaceEnergy_vol.csv
    threshold = 0.5
    variable = eta1
    mesh_volume = Volume
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 10
    percent_change = 0.05
  [../]

  #Preconditioned JFNK (default)
  solve_type = 'PJFNK'

  l_max_its = 100
  nl_abs_tol = 1e-12

  num_steps = 5000

  dtmin = 1e-2
  #dtmax = 1E4
[]

[Adaptivity]
  marker = EFM_1
  initial_steps = 3
  max_h_level = 3
  [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.1
      indicator = GJI_1
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
      variable = eta1
    [../]
  [../]
[]

[Outputs]
  file_base = 3D_GT_effect_2

  exodus = true
  interval = 1
  checkpoint = 0
  csv = true

  [./console]
    type = Console
    interval = 1
    max_rows = 10
  [../]
[]
