#This is a prototypical Hedgehog input file.  It uses Cahn-Hilliard dynamics in the split formulation.  Units are in aJ and nm.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 100
  ny = 100
  xmin = 0
  xmax = 100
  ymin = 0
  ymax = 100

  elem_type = QUAD4
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
    #  type = FunctionIC
    #  function = tanhIC
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.1
      radius = 20
      int_width = 2
      x1 = 50
      y1 = 50
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[Functions]
  [./tanhIC]
    type = ParsedFunction
    value = '0.5*tanh(x)+0.5'
  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./interface_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    off_diag_row = 'eta1 mu'
    off_diag_column = 'mu eta1'
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltBase
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    variable = total_energy
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltBaseMaterial
    AC_mobility = 1
    well_height = 7.2e-2
    kappa_AC = 2.25e-1

    order_parameter = eta1
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]

  [./SystemFreeEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]

  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]

  [./VolumeFraction]
    type = NodalVolumeFraction
 #  bubble_volume_file = 1D_interfaceEnergy_vol.csv
    threshold = 0.5
    variable = eta1
    mesh_volume = Volume
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e0
    percent_change = 0.05
  [../]

  num_steps = 1000
  dtmin = 1e-1
  dtmax = 1e2

  solve_type = 'PJFNK'
  #petsc_options_iname = '-pc_type'
  #petsc_options_value = 'lu'
  l_max_its = 100
  #nl_rel_tol = 1e-6
[]

[Outputs]
  exodus = true
  #csv = true
  interval = 10
  file_base = hedgehog_base_SplitCH
[]
