#This is an input file for Hedgehog for the 23 March 2016 CHiMaD annual meeting.  It uses Cahn-Hilliard dynamics in the split formulation.  Units are in aJ and nm.  Elasticity is included; the material is modeled as linear elastic.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 7
  ny = 7
  xmin = 0
  xmax = 150
  ymin = 0
  ymax = 150

  elem_type = QUAD4
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.01
      radius = 50
      int_width = 5
      x1 = 0
      y1 = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

#  [./disp_x]
#    order = FIRST
#    family = LAGRANGE
#  [../]

#  [./disp_y]
#    order = FIRST
#    family = LAGRANGE
#  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = FIRST
    family = MONOMIAL
  [../]

#  [./elastic_energy]
#    order = FIRST
#    family = MONOMIAL
#  [../]

  [./interface_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./total_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./grand_potential]
     order = FIRST
     family = MONOMIAL
  [../]
#
#  [./s11_aux]
#    order = FIRST
#    family = MONOMIAL
#  [../]

#  [./s12_aux]
#    order = FIRST
#    family = MONOMIAL
#  [../]

#  [./s22_aux]
#    order = FIRST
#    family = MONOMIAL
#  [../]

#  [./e11_aux]
#    order = FIRST
#    family = MONOMIAL
#  [../]

#  [./e12_aux]
#    order = FIRST
#    family = MONOMIAL
#  [../]

#  [./e22_aux]
#    order = FIRST
#    family = MONOMIAL
#  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
#  [./TensorMechanics]
#    displacements = 'disp_x disp_y'
#  [../]

  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltBase #SplitCHCobaltElasticity
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

#  [./aux_elastic_energy]
#    type = ElasticEnergy
#    variable = elastic_energy
#  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    elastic_energy = 0 # elastic_energy
    variable = total_energy
  [../]

  [./aux_grand_potential]
    type = GrandPotential
    omega_eq = 0
    OP = eta1
    variable = grand_potential
  [../]

#  [./matl_s11]
#    type = RankTwoAux
#    rank_two_tensor = stress
#    index_i = 0
#    index_j = 0
#    variable = s11_aux
#  [../]

# [./matl_s12]
#    type = RankTwoAux
#    rank_two_tensor = stress
#    index_i = 0
#    index_j = 1
#    variable = s12_aux
#  [../]

#  [./matl_s22]
#    type = RankTwoAux
#    rank_two_tensor = stress
#    index_i = 1
#    index_j = 1
#    variable = s22_aux
#  [../]

#  [./matl_e11]
#    type = RankTwoAux
#    rank_two_tensor = elastic_strain
#    index_i = 0
#    index_j = 0
#    variable = e11_aux
#  [../]

#  [./matl_e12]
#    type = RankTwoAux
#    rank_two_tensor = elastic_strain
#    index_i = 0
#    index_j = 1
#    variable = e12_aux
#  [../]

#  [./matl_e22]
#    type = RankTwoAux
#    rank_two_tensor = elastic_strain
#    index_i = 1
#    index_j = 1
#    variable = e22_aux
#  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltBaseMaterial  #CobaltTestMaterial
    AC_mobility = 5
    well_height = 7.2e-2 #5.295e-2 #7.2e-2
    kappa_AC = 2.25e-1 #2e-1 #2.25e-1

    #polynomial_order = 8
    #coefficients = '86.14585 -344.5834 573.2417 -513.6832 265.9178 -77.711 10.6834 -0.011177 0'

    #polynomial_order = 10
    #coefficients = '230.2006355 -1151.003178 2506.663551 -3120.635139 2444.046270 -1244.129167 408.0297321 -81.24549382 8.072789087 0 0'

    order_parameter = eta1
  [../]

#  [./gamma_gammaprime]
#    type = HeterogeneousLinearElasticMaterial

#    #reading C_11 C_12 C_13 C_22 C_23 C_33 C_44 C_55 C_66
#    #units of aJ/nm^3
#    Cijkl_matrix = '229 165 165 229 165 229 97.3 97.3 97.3'
#    Cijkl_precip = '272 158 158 272 158 272 162 162 162'
#    precipitate_eigenstrain = '0.0053 0.0053 0.0053 0 0 0'

#    order_parameter = eta1
#    disp_x = disp_x
#    disp_y = disp_y
  [../]
[]

[BCs]
# [./pin_nodex]
#    type = DirichletBC
#    variable = disp_x
#    value = 0.0
#    boundary = 'left'
#  [../]

# [./pin_nodey]
#    type = DirichletBC
#    variable = disp_y
#    value = 0.0
#    boundary = 'bottom'
#  [../]
[]

[Postprocessors]
  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
  [./Total_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./Bulk_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = bulk_energy
  [../]

  [./Interface_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = interface_energy
  [../]

  [./Grand_Potential_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = grand_potential
  [../]
#  [./Elastic_Energy]
#    type = ElementIntegralVariablePostprocessor
#    variable = elastic_energy
#  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./nodes]
    type = NumNodes
  [../]
 [./numDOFs]
    type = NumDOFs
    system = NL
  [../]
  [./VolumeFraction]
    type = NodalVolumeFraction
 #  bubble_volume_file = 1D_interfaceEnergy_vol.csv
    threshold = 0.5
    variable = eta1
    mesh_volume = Volume
  [../]
[]

[Adaptivity]
  marker = combo
  initial_steps = 4
  max_h_level = 4
  [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.75
      indicator = GJI_1
    [../]

#    [./EFM_2]
#      type = ErrorFractionMarker
#      coarsen = 0.05
#      refine = 0.75
#      indicator = GJI_2
#    [../]

#    [./EFM_3]
#      type = ErrorFractionMarker
#      coarsen = 0.05
#      refine = 0.75
#      indicator = GJI_3
#    [../]

    [./combo]
      type = ComboMarker
      markers = 'EFM_1' # EFM_2 EFM_3'
   [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = eta1
    [../]

#    [./GJI_2]
#     type = GradientJumpIndicator
#     variable = disp_x
#    [../]

#    [./GJI_3]
#     type = GradientJumpIndicator
#     variable = disp_y
#    [../]
#  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

  #[./TimeStepper]
  #  type = SolutionTimeAdaptiveDT
  #  dt = 1e1
  #  percent_change = 0.05
  #[../]

 [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e1
    cutback_factor = 0.25
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 1000
  dtmin = 1e-2
  #dtmax = 1e2

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type  -sub_pc_type'
  petsc_options_value = 'ksp lu'

  l_tol = 1e-4
  l_max_its = 50
  nl_max_its = 20
  nl_rel_tol = 1e-8
  nl_abs_tol = 1e-10
[]


[Outputs]
  interval = 10
  csv = true
  file_base = hedgehog_testEnergy_06012016_old
  checkpoint = true

  [./console]
    type = Console
    perf_log = true
  [../]

  [./my_exodus]
    type = Exodus
    use_problem_dimension = false
  [../]
[]
