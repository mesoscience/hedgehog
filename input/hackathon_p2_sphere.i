#This is an input file for Hackathon 2015 problem 2.

[Mesh]
  file = input/sphere2.e
[]

[GlobalParams]
  block = 2
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.5+0.05*(cos(8*(acos(z/100)))*cos(15*atan2(y,x))+(cos(12*(acos(z/100)))*cos(10*(atan2(y,x))))^2+(cos(2.5*(acos(z/100))-1.5*(atan2(y,x)))*cos(7*(acos(z/100))-2*(atan2(y,x)))))
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./n1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(1*(acos(z/100))-4)*cos(1.7*(atan2(y,x)))+cos(12*(acos(z/100)))*cos(12*(atan2(y,x)))+1.5*(cos(4.7*(acos(z/100))+4.15*(atan2(y,x)))*cos(3.2*acos(z/100)-0.5*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n2]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(2*(acos(z/100))-4)*cos(2.7*(atan2(y,x)))+cos(13*(acos(z/100)))*cos(13*(atan2(y,x)))+1.5*(cos(4.8*(acos(z/100))+4.25*(atan2(y,x)))*cos(3.3*acos(z/100)-0.6*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n3]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(3*(acos(z/100))-4)*cos(3.7*(atan2(y,x)))+cos(14*(acos(z/100)))*cos(14*(atan2(y,x)))+1.5*(cos(4.9*(acos(z/100))+4.35*(atan2(y,x)))*cos(3.4*acos(z/100)-0.7*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n4]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(4*(acos(z/100))-4)*cos(4.7*(atan2(y,x)))+cos(15*(acos(z/100)))*cos(15*(atan2(y,x)))+1.5*(cos(5.0*(acos(z/100))+4.45*(atan2(y,x)))*cos(3.5*acos(z/100)-0.8*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n5]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(5*(acos(z/100))-4)*cos(5.7*(atan2(y,x)))+cos(16*(acos(z/100)))*cos(16*(atan2(y,x)))+1.5*(cos(5.1*(acos(z/100))+4.55*(atan2(y,x)))*cos(3.6*acos(z/100)-0.9*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n6]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(6*(acos(z/100))-4)*cos(6.7*(atan2(y,x)))+cos(17*(acos(z/100)))*cos(17*(atan2(y,x)))+1.5*(cos(5.2*(acos(z/100))+4.65*(atan2(y,x)))*cos(3.7*acos(z/100)-1.0*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n7]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(7*(acos(z/100))-4)*cos(7.7*(atan2(y,x)))+cos(18*(acos(z/100)))*cos(18*(atan2(y,x)))+1.5*(cos(5.3*(acos(z/100))+4.75*(atan2(y,x)))*cos(3.8*acos(z/100)-1.1*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n8]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(8*(acos(z/100))-4)*cos(8.7*(atan2(y,x)))+cos(19*(acos(z/100)))*cos(19*(atan2(y,x)))+1.5*(cos(5.4*(acos(z/100))+4.85*(atan2(y,x)))*cos(3.9*acos(z/100)-1.2*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n9]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(9*(acos(z/100))-4)*cos(9.7*(atan2(y,x)))+cos(20*(acos(z/100)))*cos(20*(atan2(y,x)))+1.5*(cos(5.5*(acos(z/100))+4.95*(atan2(y,x)))*cos(4.0*acos(z/100)-1.3*atan2(y,x)))^2)^2
    [../]
  [../]

  [./n10]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = FunctionIC
      function = 0.1*(cos(10*(acos(z/100))-4)*cos(10.7*(atan2(y,x)))+cos(21*(acos(z/100)))*cos(21*(atan2(y,x)))+1.5*(cos(5.6*(acos(z/100))+5.05*(atan2(y,x)))*cos(4.1*acos(z/100)-1.4*atan2(y,x)))^2)^2
    [../]
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = CHCoupledSplitHackathon
    variable = c
    w = mu
    kappa_name = kappa_CH
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    well_width_factor = 2
  [../]

  [./ACBulk_n1]
    type = ACBulkPolyCoupledHackathon
    variable = n1
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 1
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta1dt]
    type = TimeDerivative
    variable = n1
  [../]

  [./ACinterface1]
    type = IsotropicACInterface
    variable = n1
  [../]
#####
  [./ACBulk_n2]
    type = ACBulkPolyCoupledHackathon
    variable = n2
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 2
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta2dt]
    type = TimeDerivative
    variable = n2
  [../]

  [./ACinterface2]
    type = IsotropicACInterface
    variable = n2
  [../]
#####
  [./ACBulk_n3]
    type = ACBulkPolyCoupledHackathon
    variable = n3
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 3
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta3dt]
    type = TimeDerivative
    variable = n3
  [../]

  [./ACinterface3]
    type = IsotropicACInterface
    variable = n3
  [../]
#####
  [./ACBulk_n4]
    type = ACBulkPolyCoupledHackathon
    variable = n4
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 4
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta4dt]
    type = TimeDerivative
    variable = n4
  [../]

  [./ACinterface4]
    type = IsotropicACInterface
    variable = n4
  [../]
#####
  [./ACBulk_n5]
    type = ACBulkPolyCoupledHackathon
    variable = n5
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 5
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta5dt]
    type = TimeDerivative
    variable = n5
  [../]

  [./ACinterface5]
    type = IsotropicACInterface
    variable = n5
  [../]
#####
  [./ACBulk_n6]
    type = ACBulkPolyCoupledHackathon
    variable = n6
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 6
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta6dt]
    type = TimeDerivative
    variable = n6
  [../]

  [./ACinterface6]
    type = IsotropicACInterface
    variable = n6
  [../]
#####
  [./ACBulk_n7]
    type = ACBulkPolyCoupledHackathon
    variable = n7
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 7
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta7dt]
    type = TimeDerivative
    variable = n7
  [../]

  [./ACinterface7]
    type = IsotropicACInterface
    variable = n7
  [../]
#####
  [./ACBulk_n8]
    type = ACBulkPolyCoupledHackathon
    variable = n8
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 8
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta8dt]
    type = TimeDerivative
    variable = n8
  [../]

  [./ACinterface8]
    type = IsotropicACInterface
    variable = n8
  [../]
#####
  [./ACBulk_n9]
    type = ACBulkPolyCoupledHackathon
    variable = n9
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 9
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta9dt]
    type = TimeDerivative
    variable = n9
  [../]

  [./ACinterface9]
    type = IsotropicACInterface
    variable = n9
  [../]
#####
  [./ACBulk_n10]
    type = ACBulkPolyCoupledHackathon
    variable = n10
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 10
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta10dt]
    type = TimeDerivative
    variable = n10
  [../]

  [./ACinterface10]
    type = IsotropicACInterface
    variable = n10
  [../]
[]

[AuxKernels]
   [./total_energy_calc]
      type = HackathonCHACEnergy
      variable = total_energy
      CH_var = c
      OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
      well_width_factor = 2
   [../]
[]

[Materials]
  [./generic]
    type = HackathonMaterial
    CH_mobility = 5
    AC_mobility = 5
    w = 5
    kappa_CH = 2
    kappa_AC = 2
    c_alpha = 0.3
    c_beta = 0.7
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./mass]
    type = ElementIntegralVariablePostprocessor
    variable = c
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
[]

[Adaptivity]
  initial_steps = 4
  max_h_level = 4
  marker = combo
 [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.25 #0.5
      indicator = GJI_1
    [../]
    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.25 #0.5
      indicator = GJI_2
    [../]
    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = c
    [../]
   [./GJI_2]
     type = GradientJumpIndicator
     variable = mu
    [../]
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e-2
    percent_change = 0.05
  [../]

  num_steps = 50000
  dtmin = 1e-2

  solve_type = 'PJFNK'
  #petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  #petsc_options_value = ' asm      lu           preonly       1'

  l_max_its = 50
  nl_max_its = 20
  nl_abs_tol = 1e-11
[]

[Outputs]
  interval = 10
  csv = true
  file_base = hackathon_p2_sphere_v2
  checkpoint = true

  [./console]
    type = Console
    perf_log = true
  [../]
  [./my_exodus]
    type = Exodus
    use_problem_dimension = false
  [../]
[]
