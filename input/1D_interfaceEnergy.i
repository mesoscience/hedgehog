#This simulation is in 1D to find the interfacial energy, aJ/nm^2 = J/m^2.
#To use this, get the equilibrium omega value first by setting omega_eq = 0 and allowing the simulation to run to equilibrium.  The omega value at either end of the simulation should be the same, and that's omega_eq.  Then run the simulation again to find the interface energy in aJ/nm^2 by supplying omega_eq to the auxkernel.  It outputs as gamma, the postprocessor.

##NB: MUST use third order hermite elements so we get the 2nd derivative!

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 100
  ny = 1
  nz = 0
  xmin = 0
  xmax = 50
  ymin = 0
  ymax = 1
  zmin = 0
  zmax = 0
  #elem_type = EDGE2
  #uniform_refine = 2
[]


[Variables]
  [./eta1]
    order = THIRD #SECOND #FIRST
    family = HERMITE #LAGRANGE
    [./InitialCondition]
    #  type = FunctionIC
    #  function = tanhIC
      variable = eta1
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.1
      radius = 25
      int_width = 2
      x1 = 0
      y1 = 0
      z1 = 0
    [../]
  [../]

  [./mu]
    order = THIRD #SECOND #FIRST
    family = HERMITE #LAGRANGE
  [../]
[]

[AuxVariables]
  [./omega]
    order = FIRST
    family = MONOMIAL
  [../]
[]

[AuxKernels]
  [./omega_calc]
    type = GrandPotential
    variable = omega
    OP = eta1
    omega_eq = 0
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltBase
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltTestMaterial #CobaltTestMaterial
    AC_mobility = 5
    well_height =  0.1 #1e-1 #0.075 #4e-2 #2.1e-2 #1.95e-2 #9.7e-2 #1.95e-1
    kappa_AC =  0.29 #0.29 #1.0 #2.85 #5.4e0 #5.8e-1 #1.1e0 #5.8e-1

    polynomial_order = 10
    coefficients = '230.2006355 -1151.003178 2506.663551 -3120.635139 2444.046270 -1244.129167 408.0297321 -81.24549382 8.072789087 0 0'

    order_parameter = eta1
  [../]
[]

[Postprocessors]
  [./dofs]
   type = NumDOFs
  [../]

  [./Gamma]
    type = ElementIntegralVariablePostprocessor
    variable = omega
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1
    percent_change = 0.05
  [../]

  #Preconditioned JFNK (default)
  solve_type = 'PJFNK'

  l_max_its = 100
  nl_abs_tol = 1e-12
  ss_check_tol = 1e-8
  #trans_ss_check = 1

  num_steps = 5000

  dtmin = 1e-2
  dtmax = 1E2
[]

#[Adaptivity]
#  marker = EFM_1
#  initial_steps = 3
#  max_h_level = 3
#  [./Markers]
#    [./EFM_1]
#      type = ErrorFractionMarker
#      coarsen = 0.075
#      refine = 0.75
#      indicator = GJI_1
#    [../]
#  [../]

#  [./Indicators]
#    [./GJI_1]
#     type = GradientJumpIndicator
#      variable = eta1
#    [../]
#  [../]
#[]

[Outputs]
  file_base = 1D_interfaceEnergy

  exodus = true
  interval = 10
  checkpoint = 0
  csv = true

  [./console]
    type = Console
    interval = 10
    max_rows = 10
  [../]
[]
