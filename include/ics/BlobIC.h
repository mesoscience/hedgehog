/******************************************************
*
*   Welcome to Hedgehog!
*
*   CHiMaD (ANL/Northwestern University)
*
*   Developer: Andrea Jokisaari
*
*   22 August 2016
*
*****************************************************/

#ifndef BLOBIC_H
#define BLOBIC_H

#include "InitialCondition.h"

// Forward Declarations
class BlobIC;
namespace libMesh { class Point; }

template<>
InputParameters validParams<BlobIC>();

class BlobIC : public InitialCondition
{
public:
  BlobIC(const InputParameters & parameters);
  virtual Real value(const Point & p);

protected:

private:
  Real _a;
  Real _b;
  Real _c;

  Real _invalue;
  Real _outvalue;

  Real _int_width;

//  Real _epsilon;
//  Real _base_value;

//  Point _q1;
//  Point _q2;
//  Point _q3;
//  Point _q4;

//  Real _k;
};

#endif //BLOBIC_H
