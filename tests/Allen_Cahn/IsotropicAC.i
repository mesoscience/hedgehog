#This is a prototypical Hedgehog input file.  It uses Allen-Cahn Dynamics.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 50
  ny = 50
  xmin = 0
  xmax = 50
  ymin = 0
  ymax = 50

  elem_type = QUAD4
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = RandomIC
      min = 0.1
      max = 0.15
    [../]
  [../]
[]

[Kernels]
  [./ACBulk]
    type = ACCobaltTest
    variable = eta1
  [../]

  [./detadt]
    type = TimeDerivative
    variable = eta1
  [../]

  [./ACinterface]
    type = IsotropicACInterface
    variable = eta1
  [../]
[]


[Materials]
  [./cobaltish]
    type = CobaltBaseMaterial
    AC_mobility = 1
    well_height = 1
    kappa_AC = 5

    order_parameter = eta1
  [../]
[]

[Executioner]
  type = Transient
  solve_type = 'PJFNK'

  dt = 1e-1
  num_steps = 10

  l_max_its = 100
[]

[Outputs]
  exodus = true
  csv = false
  interval = 1
  file_base = IsotropicAC
  checkpoint = false
[]
