#This is an input file to test interface energy via grand potential. Units are in aJ and nm.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 300
  ny = 1
  xmin = 0
  xmax = 150
  ymin = 0
  ymax = 1
[]

[Variables]
  [./eta1]
    order = THIRD #FIRST
    family = HERMITE #LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.01
      radius = 50
      int_width = 5
      x1 = 0
      y1 = 0
    [../]
  [../]

  [./mu]
    order = THIRD #FIRST
    family = HERMITE #LAGRANGE
  [../]
[]

[AuxVariables]
  [./grand_potential]
     order = FIRST
     family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltBase
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_grand_potential]
    type = GrandPotential
    omega_eq = 0
    OP = eta1
    variable = grand_potential
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltTestMaterial
    AC_mobility = 5
    well_height = 1.95e-1
    kappa_AC = 5.8e-1

    polynomial_order = 10
    coefficients = '230.2006355 -1151.003178 2506.663551 -3120.635139 2444.046270 -1244.129167 408.0297321 -81.24549382 8.072789087 0 0'

    order_parameter = eta1
  [../]
[]

[Postprocessors]
  [./Grand_Potential_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = grand_potential
  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

 [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e1
    cutback_factor = 0.25
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 5
  dtmin = 1e-2
  #dtmax = 1e2

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type  -sub_pc_type'
  petsc_options_value = 'ksp lu'

  l_tol = 1e-4
  l_max_its = 50
  nl_max_its = 20
  nl_rel_tol = 1e-8
  nl_abs_tol = 1e-10
[]

[Outputs]
  interval = 1
  csv = false
  file_base = 1D
  checkpoint = false

  [./console]
    type = Console
    perf_log = true
  [../]

  [./my_exodus]
    type = Exodus
    use_problem_dimension = true
  [../]
[]
