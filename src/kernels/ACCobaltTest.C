/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   3 February 2016
 *
 *****************************************************/

#include "ACCobaltTest.h"

template<>
InputParameters validParams<ACCobaltTest>()
{
  InputParameters params = validParams<IsotropicACBulk>(); //where should this come from, KernelValue or ACBulk?
  //add things as necessary here

  return params;
}

ACCobaltTest::ACCobaltTest(const InputParameters & parameters) :
    IsotropicACBulk(parameters),
    _df_dOP(getMaterialProperty<Real>("df_bulk_dOP")),
    _d2f_dOP2(getMaterialProperty<Real>("d2f_bulk_dOP2"))
{
}

Real
ACCobaltTest::computeDFDOP(PFFunctionType type)
{
  switch (type)
  {
    case Residual:
      return _df_dOP[_qp];

    case Jacobian:
      return _phi[_j][_qp]*_d2f_dOP2[_qp];
  }

  mooseError("Invalid type passed in");
}
