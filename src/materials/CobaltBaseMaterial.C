/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   3 February 2016
 *
 *****************************************************/

#include "CobaltBaseMaterial.h"

template<>
InputParameters validParams<CobaltBaseMaterial>()
{
  InputParameters params = validParams<Material>();

  params.addRequiredParam<Real>("AC_mobility", "isotropic Allen-Cahn kinetic coefficient");
  params.addRequiredParam<Real>("well_height", "coefficient in frot of double-well function");
  params.addRequiredParam<Real>("kappa_AC", "gradient energy coefficient for eta");

  params.addRequiredCoupledVar("order_parameter", "order parameter variable");

  return params;
}

CobaltBaseMaterial::CobaltBaseMaterial(const InputParameters & parameters) :
    Material(parameters),
    _L(declareProperty<Real>("AC_mobility")),
    _kappa_AC(declareProperty<Real>("kappa_AC")),
    _fbulk(declareProperty<Real>("f_bulk")),
    _dfbulkdOP(declareProperty<Real>("df_bulk_dOP")),
    _d2fbulkdOP2(declareProperty<Real>("d2f_bulk_dOP2")),
    _L_param(getParam<Real>("AC_mobility")),
    _w_param(getParam<Real>("well_height")),
    _kappa_AC_param(getParam<Real>("kappa_AC")),
    _OP(coupledValue("order_parameter"))
{
}

void
CobaltBaseMaterial::computeQpProperties()
{
  _L[_qp] = _L_param;
  _kappa_AC[_qp] = _kappa_AC_param;

  //chemical free energy
  _fbulk[_qp] = _w_param*( _OP[_qp]*_OP[_qp]*(1-_OP[_qp])*(1-_OP[_qp]));

  _dfbulkdOP[_qp] = _w_param*(2*_OP[_qp] - 6*_OP[_qp]*_OP[_qp] + 4*_OP[_qp]*_OP[_qp]*_OP[_qp]);

  _d2fbulkdOP2[_qp] =_w_param*(2 - 12*_OP[_qp] + 12*_OP[_qp]*_OP[_qp]);
}
