/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   11 February 2016
 *
 *****************************************************/

#include "ElasticEnergy.h"

template<>
InputParameters validParams<ElasticEnergy>()
{
  InputParameters params = validParams<AuxKernel>();

  return params;
}

ElasticEnergy::ElasticEnergy(const InputParameters & parameters) :
    AuxKernel(parameters),
    _felastic(getMaterialProperty<Real>("f_elastic"))
{
}

Real
ElasticEnergy::computeValue()
{
  return _felastic[_qp];
}
