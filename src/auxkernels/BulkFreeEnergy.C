/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   4 February 2016
 *
 *****************************************************/

#include "BulkFreeEnergy.h"

template<>
InputParameters validParams<BulkFreeEnergy>()
{
  InputParameters params = validParams<AuxKernel>();

  return params;
}

BulkFreeEnergy::BulkFreeEnergy(const InputParameters & parameters) :
    AuxKernel(parameters),
    _fbulk(getMaterialProperty<Real>("f_bulk"))
{
}

Real
BulkFreeEnergy::computeValue()
{
  return _fbulk[_qp];
}
